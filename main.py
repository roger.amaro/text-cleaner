# -*- coding: utf-8 -*-
from __future__ import with_statement

from glob import glob

import chardet
import re
import os
import os.path
from datetime import datetime
from pathlib import Path
from tkinter import *
from tkinter import filedialog, messagebox, DoubleVar, Toplevel, Menu
from tkinter import ttk
import codecs


class CustomText(Text):
    def __init__(self, *args, **kwargs):
        """A text widget that report on internal widget commands"""
        Text.__init__(self, *args, **kwargs)
        self.bind('<Control-c>', self.copy)
        self.bind('<Control-x>', self.cut)
        self.bind('<Control-v>', self.paste)

        # create a proxy for the underlying widget
        self._orig = self._w + "_orig"
        self.tk.call("rename", self._w, self._orig)
        self.tk.createcommand(self._w, self._proxy)

    def _proxy(self, command, *args):
        cmd = (self._orig, command) + args
        result = self.tk.call(cmd)

        if command in ("insert", "delete", "replace"):
            self.event_generate("<<TextModified>>")

        return result

    def copy(self, event=None):
        self.clipboard_clear()
        text = self.get("sel.first", "sel.last")
        self.clipboard_append(text)

    def cut(self, event):
        self.copy()
        self.delete("sel.first", "sel.last")

    def paste(self, event):
        text = self.selection_get(selection='CLIPBOARD')
        self.insert('insert', text)


def index_array_string(full_text, string):
    results = []
    lines = full_text.splitlines()

    for idx, val in enumerate(lines):
        for match in re.finditer(string, val):
            results.append(
                dict(
                    word=string,
                    start=f"{idx + 1}.{match.start()}",
                    end=f"{idx + 1}.{match.end()}"
                )
            )

    return results


def rClicker(e):
    """ right click context menu for all Tk Entry and Text widgets
    """

    try:
        def rClick_Copy(e, apnd=0):
            e.widget.event_generate('<Control-c>')

        def rClick_Cut(e):
            e.widget.event_generate('<Control-x>')

        def rClick_Paste(e):
            e.widget.event_generate('<Control-v>')

        e.widget.focus()

        nclst = [
            (' Cut', lambda e=e: rClick_Cut(e)),
            (' Copy', lambda e=e: rClick_Copy(e)),
            (' Paste', lambda e=e: rClick_Paste(e)),
        ]

        rmenu = Menu(None, tearoff=0, takefocus=0)

        for (txt, cmd) in nclst:
            rmenu.add_command(label=txt, command=cmd)

        rmenu.tk_popup(e.x_root + 60, e.y_root + 10, entry="0")

    except TclError as err:
        print(err)


def callback_select_all(event, tp_lvl):
    # select text after 50ms
    tp_lvl.after(50, lambda: event.widget.select_range(0, 'end'))


def remove_tab(text):
    lines = [line.strip('\n') for line in text.splitlines()]
    lines = [x for x in lines if x != ""]
    formatted_text = []
    for num, line in enumerate(lines):
        line = line.strip()
        formatted_text.append(line)

    text = '\n'.join(formatted_text)
    return text.replace("\t", "")


def open_file(path_to_file):
    rawdata = open(path_to_file, "rb").read()
    encode = chardet.detect(rawdata)

    if path_to_file.endswith(".txt"):
        try:
            file = codecs.open(path_to_file, 'rb', encoding=encode.get("encoding"))
            text = file.read()
            file.close()
            return text
        except UnicodeDecodeError as err:
            return False


class App:
    path_to_origem = ""
    path_to_destino = ""

    def __init__(self, master):
        self.replace_to_str = StringVar()
        self.regex_entry = None
        self.text_regex_highlight = None
        self.text_in_test_string = StringVar()
        self.matches = StringVar()
        self.text_matches = None
        self.ignore_case = BooleanVar()
        self.multiline = BooleanVar()
        self.sv = StringVar()
        self.popup = None

        self.regex_check_box = BooleanVar()

        self.regex_info = StringVar()
        self.regex_replace = StringVar()
        self.remove_tab = BooleanVar()
        self.format_utf_8 = BooleanVar()
        self.remove_hifen = BooleanVar()
        self.remove_aspas_inglesas = BooleanVar()
        self.remove_travessao = BooleanVar()

        self.frame = Frame(master)
        self.frame.grid(row=1, pady=3)

        self.frame2 = Frame(master)
        self.frame2.grid(row=0, pady=10)

        self.frame3 = Frame(master)
        self.frame3.grid(row=2, pady=40)

        self.origem_text = Label(self.frame2, text="Origem")
        self.origem_text.grid(row=0, column=0, padx=1)

        self.text_label_origem = Entry(self.frame2, width=55)
        self.text_label_origem.grid(row=0, column=1, padx=5)

        self.browse_origem = Button(self.frame2, text="...", command=self.browse_button_origem, height=1)
        self.browse_origem.grid(row=0, column=2, padx=10)

        self.destino_text = Label(self.frame2, text="Destino")
        self.destino_text.grid(row=1, column=0, padx=10)

        self.text_label_destino = Entry(self.frame2, width=55)
        self.text_label_destino.grid(row=1, column=1, padx=5)

        self.browse_destino = Button(self.frame2, text="...", command=self.browse_button_destino)
        self.browse_destino.grid(row=1, column=2, padx=10)

        self.check_box_remove_tab = Checkbutton(self.frame, text="remover tabulacões", variable=self.remove_tab)
        self.check_box_remove_tab.grid(row=1, column=1, pady=30, padx=(10, 5), sticky=W)

        # self.check_box_utf_8 = Checkbutton(self.frame, text="aplicar utf-8", variable=self.format_utf_8)
        # self.check_box_utf_8.grid(row=2, column=1)

        self.check_box_hifen = Checkbutton(self.frame, text="remover hifen", variable=self.remove_hifen)
        self.check_box_hifen.grid(row=1, column=0, padx=(10, 20), pady=30)

        self.remove_aspas_ing = Checkbutton(self.frame, text="substituir aspas inglesas",
                                            variable=self.remove_aspas_inglesas)
        self.remove_aspas_ing.grid(row=1, column=2, pady=5, padx=10)

        # if self.sv.get() and self.replace_to_str.get():
        #     Checkbutton(
        #         master=self.frame,
        #         text="regex replace",
        #         variable=self.regex_check_box
        #     ).grid(row=4, column=1, pady=5, padx=10)

        # self.remove_trevssao = Checkbutton(self.frame, text="remove travessão", variable=self.remove_travessao)
        # self.remove_trevssao.grid(row=3, column=1, pady=5, padx=10)

        self.start_btn = Button(self.frame3, text="Iniciar", command=self.start_process)
        self.start_btn.grid(row=4, column=0)

        self.button_regex = Button(self.frame3, text="Regex", command=self.regex_init)
        self.button_regex.grid(row=4, column=1, padx=10)

    def check_regx(self, *args):
        for tag in self.text_regex_highlight.tag_names():
            self.text_regex_highlight.tag_delete(tag)

        if self.sv.get() and self.text_regex_highlight.get("1.0", END):
            rgx = self.sv.get()
            try:
                re.compile(rgx)
            except re.error:
                self.text_matches['text'] = 'matches: 0'
                self.text_matches.config(bg="red")
                self.popup.update()

                return

            matched = re.findall(rgx, self.text_regex_highlight.get("1.0", END))

            if self.ignore_case.get() and not self.multiline.get():
                matched = re.findall(self.sv.get(), self.text_regex_highlight.get("1.0", END), re.IGNORECASE)

            if self.ignore_case.get() and self.multiline.get():
                matched = re.findall(
                    self.sv.get(),
                    self.text_regex_highlight.get("1.0", END),
                    flags=re.IGNORECASE | re.MULTILINE)

            if self.multiline.get() and not self.ignore_case.get():
                matched = re.findall(self.sv.get(), self.text_regex_highlight.get("1.0", END), re.MULTILINE)

            is_match = bool(matched)
            if not is_match:
                self.text_matches['text'] = 'matches: 0'
                self.text_matches.config(bg="red")
                self.popup.update()
                return
            else:
                full_text = self.text_regex_highlight.get("1.0", END)

                for word in matched:
                    # lines = [line.split() for line in full_text.splitlines()]
                    if len(full_text.splitlines()) > 1 and not self.multiline.get():
                        self.multiline.set(True)

                    index_highlighting = index_array_string(full_text, word)
                    for idx, indexed_word in enumerate(index_highlighting):
                        tag_name = f"{indexed_word}_{idx}"
                        self.text_regex_highlight.tag_add(tag_name,
                                                          indexed_word.get("start"),
                                                          indexed_word.get("end"))

                        self.text_regex_highlight.tag_config(tag_name,
                                                             background="yellow",
                                                             foreground="blue")

                self.text_matches['text'] = f'matches: {len(matched)}'
                self.text_matches.config(bg="green")
                self.popup.update()
                return

    def check_regex_fields(self):
        if not self.sv.get() or not self.replace_to_str.get():
            messagebox.showerror("Invalid Null Field", "replace or regex can't be None")
            return
        self.popup.destroy()
        self.popup = None
        self.frame.master.update()

    def cancel_rgx(self):
        self.popup.destroy()
        self.popup = None
        self.frame.master.update()
        self.sv = StringVar()
        self.replace_to_str = StringVar()

    def regex_init(self):

        if self.popup is None:
            self.popup = Toplevel()
        # self.popup.resizable(False, False)
        self.popup.geometry("415x560+100+200")
        self.popup.title("Entrada de Regex")

        frame_1 = Frame(self.popup)
        frame_2 = Frame(self.popup)
        frame_2.grid(row=1, column=0)
        frame_1.grid(row=0, column=0, sticky=W)
        frame_3 = Frame(self.popup)
        frame_3.grid(row=2, column=0, sticky=W)
        frame_4 = Frame(self.popup)
        frame_4.grid(row=3, column=0, sticky=W)
        frame_5 = Frame(self.popup)
        frame_5.grid(row=4, column=0, sticky=W)
        frame_6 = Frame(self.popup)
        frame_6.grid(row=5, column=0, sticky=W)
        frame_7 = Frame(self.popup)
        frame_7.grid(row=6, column=0, sticky=W)
        frame_8 = Frame(self.popup)
        frame_8.grid(row=7, column=0, sticky=W)

        Label(frame_1, text="Regular expression:").grid(row=0, column=0, pady=0, padx=10, sticky=W)

        self.sv.trace("w", self.check_regx)

        self.regex_entry = Entry(frame_2,
                                 width=48,
                                 justify=LEFT,
                                 textvariable=self.sv,
                                 )
        self.regex_entry.grid(row=1, column=0, pady=5, padx=10)

        testing_string = Label(
            master=frame_3,
            text="Testing string:"
        )
        testing_string.grid(row=2, column=0, pady=10, padx=10)

        scrollbar = Scrollbar(frame_4)

        self.text_regex_highlight = CustomText(
            master=frame_4,
            height=15,
            width=45,
            yscrollcommand=scrollbar.set
        )

        self.text_regex_highlight.bind("<<TextModified>>", self.check_regx)

        scrollbar.config(
            command=self.text_regex_highlight.yview,
            elementborderwidth=-2,
            width=14
        )

        self.text_regex_highlight.grid(row=3, column=0, padx=(10, 4))
        scrollbar.grid(row=3, column=1, sticky=NS)

        self.chk_box_ignore_case = Checkbutton(
            master=frame_5,
            text="Ignore case",
            variable=self.ignore_case,
            command=self.check_regx
        )
        self.chk_box_ignore_case.grid(row=4, column=0, pady=10)

        self.chk_box_multiline = Checkbutton(
            master=frame_5,
            text="Multiline",
            variable=self.multiline,
            command=self.check_regx
        )
        self.chk_box_multiline.grid(row=4, column=1, pady=10)

        self.text_matches = Label(
            master=frame_6,
            text='matches: 0',
        )
        self.text_matches.grid(row=4, column=1, pady=5, padx=10, sticky=S)

        replace_to = Entry(
            master=frame_7,
            textvariable=self.replace_to_str
        )
        replace_to.grid(row=5, column=1)
        replace_to.event_add('<<Paste>>', '<Control-q>')

        Label(
            master=frame_7,
            text="Replace to:"
        ).grid(row=5, column=0, pady=10, padx=10)

        ttk.Separator(frame_8, orient=HORIZONTAL).grid(row=1, column=0, columnspan=4, ipadx=195, padx=10, pady=15)

        btn_ok = Button(
            master=frame_8,
            anchor="w",
            text="Ok",
            command=self.check_regex_fields
        )
        btn_ok.grid(row=5, column=0, padx=(0, 80), sticky=E)

        btn_cancel = Button(
            master=frame_8,
            anchor="w",
            text="cancel",
            command=self.cancel_rgx
        )
        btn_cancel.grid(row=5, column=0, padx=(300, 0), sticky=E)
        self.popup.protocol('WM_DELETE_WINDOW', self.cancel_rgx)

    def browse_button_origem(self):
        directory_name = filedialog.askdirectory()
        self.path_to_origem = directory_name
        self.text_label_origem.insert(0, directory_name)

    def browse_button_destino(self):
        directory_name = filedialog.askdirectory()
        self.path_to_destino = directory_name
        self.text_label_destino.insert(0, directory_name)

    def start_process(self):
        unprocessed_file = []
        files_for_process = []

        if not self.text_label_origem.get():
            messagebox.showwarning("Warning", "a pasta de origem dos dados não pode ser nula!")
            return

        if (not self.remove_tab.get() and
                not self.format_utf_8.get() and
                not self.remove_hifen.get() and
                not self.remove_aspas_inglesas.get() and
                not self.remove_travessao.get() and
                not self.regex_info.get() and
                not self.replace_to_str.get() and
                not self.sv.get()):
            messagebox.showwarning("Warning", "Pelo menos uma checkbox deve estar selecionada ou um regex deve ser "
                                              "fornecido!")
            return

        if not self.text_label_destino.get():
            self.path_to_destino = self.path_to_origem

        types = ["*.txt"]

        for ext in types:
            for path in Path(self.path_to_origem).rglob(ext):
                files_for_process.append(path)


        if len(files_for_process) == 0:
            messagebox.showerror("Error", f"não foram encontrados arquivos")
            return

        popup = Toplevel()
        Label(popup, text="loading").grid(row=0, column=0)

        progress = 0
        progress_var = DoubleVar()
        progress_bar = ttk.Progressbar(popup, variable=progress_var, maximum=100)
        progress_bar.grid(row=1, column=0)
        popup.pack_slaves()
        progress_step = float(100.0 / len(files_for_process))

        for filename in files_for_process:
            popup.update()
            progress += progress_step
            progress_var.set(progress)

            text = open_file(filename.as_posix())
            if not text:
                unprocessed_file.append(filename.name)
                continue
            if self.remove_tab.get():
                text = remove_tab(text)

            if self.sv.get() and self.replace_to_str.get():
                text = re.sub(self.sv.get(), self.replace_to_str.get(), text)

            if self.remove_aspas_inglesas.get():
                text = text.replace('“', '"')
                text = text.replace('”', '"')

                text = text.replace("`", "'")
                text = text.replace('’', "'")

            # if self.remove_travessao.get():
            #     text = text.replace("—", "")

            if self.remove_hifen.get():

                lines = [line.strip('\n') for line in text.splitlines()]
                lines = [x for x in lines if x != ""]
                for num, line in enumerate(lines):

                    if line.endswith('-'):
                        # the end of the word is at the start of next line
                        try:
                            end = lines[num + 1].split()[0]
                            # we remove the - and append the end of the word
                            lines[num] = line[:-1] + end
                            # and remove the end of the word and possibly the
                            # following space from the next line
                            lines[num + 1] = lines[num + 1][len(end) + 1:]
                        except IndexError as err:
                            raise Exception(err)

                text = '\n'.join(lines)
            if not os.path.exists(self.path_to_destino + "/output/"):
                os.makedirs(self.path_to_destino + "/output/")

            path_to_save = "/output" + filename.as_posix().replace(self.path_to_origem, "")
            path_to_save = self.path_to_destino + path_to_save[::-1].split("/", 1)[1][::-1] + "/"

            if not os.path.exists(path_to_save):
                os.makedirs(path_to_save)

            try:
                file_name = filename.name
                file_name = file_name.replace(" ", "_")

                file_finish = codecs.open(path_to_save + file_name, "wb", encoding="utf-8")
                file_finish.write(text)
                file_finish.close()
            except Exception:
                unprocessed_file.append(filename)

        if not unprocessed_file:
            messagebox.showinfo("Fim", f" {len(files_for_process)} arquivos processados com sucesso")
        else:
            messagebox.showinfo("Fim",
                                f" {len(unprocessed_file)} de {len(files_for_process)} arquivos não puderam ser processados!! \n" \
                                f"uma lista de arquivos não processados será gerada em:\n " \
                                f"/output/unprocessed/unprocessed_files_name.txt")
            if not os.path.exists(self.path_to_destino + "/output/unprocessed/"):
                os.makedirs(self.path_to_destino + "/output/unprocessed/")
            file = open(self.path_to_destino + f"/output/unprocessed/{datetime.utcnow().isoformat()}\
            unprocessed_files_name.txt", 'w')
            for line in unprocessed_file:
                file.write(line + "\n")
            file.close()
        popup.destroy()


root = Tk()
root.geometry("600x300")
root.eval('tk::PlaceWindow . center')
root.title("Text Cleaner")
app = App(root)

root.mainloop()
